import 'package:flutter/material.dart';
import 'package:workgroups_app/menucontrol.dart';

class Biodata extends StatefulWidget {
  Biodata({Key key}) : super(key: key);

  @override
  _BiodataState createState() => _BiodataState();
}

class _BiodataState extends State<Biodata> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "Profile",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF00B0FF),
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
            SizedBox(
              height: 55.0,
            ),
            Image.asset(
              'assets/profile.jpg',
              height: 150.0,
              width: 150.0,
            ),
            SizedBox(
              height: 55.0,
            ),
            Table(
              // border: TableBorder.all(),
              children: [
                TableRow(children: [
                  Column(children: [
                    Text(
                      'Name : ',
                      textAlign: TextAlign.left,
                    )
                  ]),
                  Column(children: [
                    Text(
                      'Muhammad Fajar Wibisono ',
                      textAlign: TextAlign.left,
                    )
                  ]),
                ]),
                TableRow(children: [
                  Column(children: [
                    Text(
                      'Hobby : ',
                      textAlign: TextAlign.left,
                    )
                  ]),
                  Column(children: [
                    Text(
                      'Listening Tech Podcast        ',
                      textAlign: TextAlign.left,
                    )
                  ]),
                ]),
                TableRow(children: [
                  Column(children: [
                    Text(
                      'Birthday Information : ',
                      textAlign: TextAlign.left,
                    )
                  ]),
                  Column(children: [
                    Text(
                      'Bandung, 13 December 1997',
                      textAlign: TextAlign.left,
                    )
                  ]),
                ]),
                TableRow(children: [
                  Column(children: [
                    Text(
                      'Address : ',
                      textAlign: TextAlign.left,
                    )
                  ]),
                  Column(children: [
                    Text(
                      'Bumi Parahiyangan Kencana',
                      textAlign: TextAlign.left,
                    )
                  ]),
                ]),
              ],
            ),
            SizedBox(
              height: 55.0,
            ),
            FlatButton(
              minWidth: 100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  side: BorderSide(color: Color(0xFF00B0FF))),
              child: Text(
                'Back',
                style: TextStyle(fontSize: 20.0),
              ),
              color: Colors.white,
              textColor: Color(0xFF00B0FF),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => MyStatefulWidget(),
                ));
              },
            )
          ],
        ),
      ),
    );
  }
}
