import 'package:flutter/material.dart';
import 'package:workgroups_app/biodata.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Dashboard extends StatefulWidget {
  String txtUsername;
  Dashboard({Key key, @required this.txtUsername}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState(txtUsername);
}

class _DashboardState extends State<Dashboard> {
  String txtUsername;
  _DashboardState(this.txtUsername);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ListView(
          children: <Widget>[
            Text(
              "HELLO MATES ",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF00B0FF),
                fontWeight: FontWeight.bold,
                fontSize: 30.0,
              ),
            ),
            Text(
              "THIS IS YOUR DASHBOARD",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF00B0FF),
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Image.asset(
              'assets/profile.jpg',
              height: 150.0,
              width: 150.0,
            ),
            SizedBox(
              height: 30.0,
            ),
            Text(
              "Your Assignment",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF00B0FF),
                fontWeight: FontWeight.bold,
                fontSize: 20.0,
              ),
            ),
            Row(
              children: <Widget>[
                Card(
                  color: Color(0xFF00B0FF),
                  elevation: 10,
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10.0,
                        ),
                        SvgPicture.asset(
                          'assets/task.svg',
                          height: 100.0,
                          width: 100.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Mobile Prog 1",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 13.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Card(
                  color: Color(0xFF00B0FF),
                  elevation: 10,
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        SizedBox(
                          height: 10.0,
                        ),
                        SvgPicture.asset(
                          'assets/task.svg',
                          height: 100.0,
                          width: 100.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          "Mobile Prog 2",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 13.0,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 30.0,
            ),
            FlatButton(
              minWidth: 100,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0),
                  side: BorderSide(color: Color(0xFF00B0FF))),
              child: Text(
                'Check Profile',
                style: TextStyle(fontSize: 20.0),
              ),
              color: Colors.white,
              textColor: Color(0xFF00B0FF),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => Biodata(),
                ));
              },
            )
          ],
        ),
      ),
    );
  }
}
