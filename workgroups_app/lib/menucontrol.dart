import 'package:flutter/material.dart';
import 'package:workgroups_app/dashboardbackup.dart';
import 'package:workgroups_app/classmate.dart';
import 'package:workgroups_app/login.dart';

class MyStatefulWidget extends StatefulWidget {
  String txtUsername;
  MyStatefulWidget({Key key, this.txtUsername}) : super(key: key);
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState(txtUsername);
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;
  static String txtUsername;
  _MyStatefulWidgetState(txtUsername);

  final List<Widget> _widgetOptions = [
    Dashboard(txtUsername: "M FAJAR WIBISONO"),
    Classmates(),
  ];
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static String test = MyStatefulWidget().txtUsername.toString();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions[_selectedIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: 'Clasemate',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.logout),
            label: 'Logout',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Color(0xFF00B0FF),
        onTap: _onItemTapped,
      ),
    );
  }
}
